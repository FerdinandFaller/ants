using UnityEngine;

/// <summary>
/// The used GFX without rotation has to come in from Left.
/// </summary>
[ExecuteInEditMode]
public class Tunnel : Portal
{
	public Direction In;
	
	public override void Init ()
	{
		base.Init();
		
		UpdateGFX(DetermineDirection());
	}
	
	private Direction DetermineDirection()
	{
		var returnVar = Direction.Left;
		
		if(x == 0)
		{
		}
		else if(x == TileMap.Width - 1)
		{
			returnVar = Direction.Right;
		}
		
		if(y == 0)
		{
			returnVar = Direction.Down;
		}
		else if(y == TileMap.Height - 1)
		{
			returnVar = Direction.Up;
		}
		
		return returnVar;
	}
	
	
	private void UpdateGFX(Direction inDir)
	{
		var zAngle = 0;
		
		switch(inDir)
		{
			case Direction.Left:
			{
				zAngle = 0;
			}break;
			
			case Direction.Right:
			{
				zAngle = 180;
			}break;
			
			case Direction.Up:
			{
				zAngle = -90;
			}break;
			
			case Direction.Down:
			{
				zAngle = 90;
			}break;
		}
		
		transform.Rotate(-transform.rotation.eulerAngles);
		transform.rotation = Quaternion.Euler(0, 0, zAngle);
	}
}
