using UnityEngine;


[ExecuteInEditMode]
public class TongueBase : TileIn2
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromDown = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		
		BaseSpriteId = "TongueBase";
		
		base.Init();
	}
}