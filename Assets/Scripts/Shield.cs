using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class Shield : Tile 
{
	public bool IsRight;
	public bool IsLeft;
	public bool IsUp;
	public bool IsDown;
	
	private GameObject _left;
	private GameObject _right;
	private GameObject _up;
	private GameObject _down;
	
	
	public override void Init()
	{
		_right = transform.FindChild("Right").gameObject;
		_left = transform.FindChild("Left").gameObject;
		_up = transform.FindChild("Up").gameObject;
		_down = transform.FindChild("Down").gameObject;
		
		_right.active = IsRight;
		_left.active = IsLeft;
		_up.active = IsUp;
		_down.active = IsDown;
		
		CanBeEnteredFromRight = !IsRight;
		CanBeEnteredFromLeft = !IsLeft;
		CanBeEnteredFromUp = !IsUp;
		CanBeEnteredFromDown = !IsDown;
	}
	
	
	public void SetDirection(Direction dir, bool shouldBeOn)
	{
		switch(dir)
		{
			case Direction.Left:
			{
				IsLeft = shouldBeOn;
				if(_left == null) _left = transform.FindChild("Left").gameObject;
				_left.active = shouldBeOn;
			}break;
			
			case Direction.Right:
			{
				IsRight = shouldBeOn;
				if(_left == null) _right = transform.FindChild("Right").gameObject;
				_right.active = shouldBeOn;
			}break;
			
			case Direction.Up:
			{
				IsUp = shouldBeOn;
				if(_left == null) _up = transform.FindChild("Up").gameObject;
				_up.active = shouldBeOn;
			}break;
			
			case Direction.Down:
			{
				IsDown = shouldBeOn;
				if(_left == null) _down = transform.FindChild("Down").gameObject;
				_down.active = shouldBeOn;
			}break;
			
			default:
			{
				
			}break;
		}
		
		CanBeEnteredFromRight = !IsRight;
		CanBeEnteredFromLeft = !IsLeft;
		CanBeEnteredFromUp = !IsUp;
		CanBeEnteredFromDown = !IsDown;
	}
	
	public bool IsDirectionEnabled(Direction dir)
	{
		if(dir == Direction.Left && IsLeft) return true;
		if(dir == Direction.Right && IsRight) return true;
		if(dir == Direction.Up && IsUp) return true;
		if(dir == Direction.Down && IsDown) return true;
		
		return false;
	}
}
