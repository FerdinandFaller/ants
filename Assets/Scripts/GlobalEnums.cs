using UnityEngine;


public enum Direction
{
	Left = 0,
	Right,
	Up,
	Down,
}

public enum GameState
{
	Ingame = 0,
}

public enum CommandType
{
	Null = 0,
	TileMovement,
	TileRemoval,
	TileAdding,
	TileManipulation,
	Input,
	Macro,
	CameraMovement,
}

public enum InputType
{
	Mouse = 0,
	Keyboard,
	Touch,
}



public static class GlobalEnums
{
	public static Direction GetOppositeOfDirection(Direction direction)
	{
		var returnVar = direction;
		
		switch(direction)
		{
			case Direction.Left:
			{
				returnVar = Direction.Right;
			}break;
			
			case Direction.Right:
			{
				returnVar = Direction.Left;
			}break;
			
			case Direction.Up:
			{
				returnVar = Direction.Down;
			}break;
			
			case Direction.Down:
			{
				returnVar = Direction.Up;
			}break;
		}
		
		return returnVar;
	}
}