using UnityEngine;
using System.Collections.Generic;


[ExecuteInEditMode]
public class TongueTip : TileIn1 
{
	public Direction StartLookDirection = Direction.Right;
	public GameObject TongueBasePrefab;
	
	public int MovementsCount = 0;
	
	public TileMovement LastMove;
	
	public bool IsInHole;
	
	private Direction _lookDirection;
	public Direction LookDirection
	{ 
		get{return _lookDirection;}
		set
		{
			if(_lookDirection != value)
			{
				In = GlobalEnums.GetOppositeOfDirection(value);
				UpdateGFX(In);
			}
			
			_lookDirection = value;
		}
	}
	
	public event System.Action<TileMovement, int> MoveEvent;
	public event System.Action<TileMovement, int> MoveUndoneEvent;
	
	
	public override void Init()
	{
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		
		IsWalkable = true;
		CanBeEnteredFromDown = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		
		if(IsInHole)
		{
			_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTipInHole");
		}
		else
		{
			_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTip");
		}
		
		base.Init();
	}
	
	public void FireMoveEvent(TileMovement move)
	{
		if(MoveEvent != null)
		{
			MoveEvent(move, MovementsCount);
		}
	}
	
	public void FireMoveUndoneEvent(TileMovement move)
	{
		if(MoveUndoneEvent != null)
		{
			MoveUndoneEvent(move, MovementsCount);
		}
	}
	
	public void ChangeAppearenceToLookOutOfHole()
	{
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTipInHole");
	}
	
	public void ChangeAppearanceToNormal()
	{
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTip");
	}
	
	void Update()
	{
#if UNITY_EDITOR
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		if(IsInHole)
		{
			_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTipInHole");
		}
		else
		{
			_sprite.spriteId = _sprite.GetSpriteIdByName("TongueTip");
		}
#endif
	}
}
