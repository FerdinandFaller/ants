using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// Tile map. This Scipts has to be executed before the BaseTileMapObject Script.
/// It is important that the Depth in all tk2DTileMap Layers is int compatible.
/// </summary>
[ExecuteInEditMode]
public class TileMap : MonoBehaviour
{
	public GameObject tk2dTileMapGO;
	
	public static event System.Action TileMapInitializationCompleteEvent;
	
	private static List<BaseTileMapObject> _tilesList;
	private static SortedList<string, TileMapLayer> _layerList;
	
	private static tk2dTileMap _tk2dTileMap;
	
	public static int Width {get; protected set;}
	public static int Height {get; protected set;}
	
	public static TileMap Instance {get; private set;} 
	
	public string TongueTipLayerName = "TongueTip";
	public string TongueBaseLayerName = "TongueBase";
	public string ForegroundLayerName = "Foreground";
	public string BackgroundLayerName = "Background";
	public string ObstaclesLayerName = "Obstacles";
	public string AntsLayerName = "Ants";
	public string PortalsLayerName = "Portals";
	
	
	#region Unity Methods
	
	void Awake()
	{
		if(_layerList == null) _layerList = new SortedList<string, TileMapLayer>();
		if(_tilesList == null) _tilesList = new List<BaseTileMapObject>();
		
		Instance = this;
		
		_tk2dTileMap = tk2dTileMapGO.GetComponent<tk2dTileMap>();
		
		Width = _tk2dTileMap.width;
		Height = _tk2dTileMap.height;
		
		// Setup all Layers
		foreach(var tileLayer in _tk2dTileMap.data.tileMapLayers)
		{
			var z = 0f;
			foreach(var temp in _tk2dTileMap.Layers)
			{
				if(tileLayer.name == temp.gameObject.name)
				{
					z = temp.gameObject.transform.position.z;
					break;
				}
			}
			
			var layer = new TileMapLayer(tileLayer.name, Width, Height, (int)z);
			if(!_layerList.ContainsKey(tileLayer.name))
			{
				_layerList.Add(tileLayer.name, layer);
			}
			
#if UNITY_EDITOR
			Debug.Log("Added Layer: " + tileLayer.name + " depth: " + ((int)z).ToString());
#endif
		}
	}
	
	void Start()
	{
		// By now all Tiles from the tk2D TileMap have registered themselves
		foreach(var tile in _tilesList)
		{
			AddTile(tile);
		}
		
		if(TileMapInitializationCompleteEvent != null)
		{
			TileMapInitializationCompleteEvent();
		}
	}
	
	#endregion // Unity Methods
	
	#region Adding/Removing Methods
	
	public static void RegisterTile(BaseTileMapObject tile)
	{
		if(_tilesList == null) _tilesList = new List<BaseTileMapObject>();
		
		if(!_tilesList.Contains(tile))
		{
			//Debug.Log ("Registering Tile: " + tile.GetType().ToString());
			_tilesList.Add(tile);
		}
		else
		{
			Debug.LogWarning("Can't register Tile. Already contained in _tilesList!");
		}
	}
	
	public static bool AddTile(BaseTileMapObject tile)
	{
		if(_layerList == null) throw new System.ArgumentNullException();
		
		var returnVar = true;
		
		// Tk2D
		if(tile.x == -1 || tile.y == -1)
		{
			int x, y;
			var layerName = GetLayerNameFromLayerDepth((int)tile.transform.position.z);
			_tk2dTileMap.GetTileAtPosition(tile.transform.position, out x, out y);
			
			if(!IsIndexFree(layerName, x, y)) return false;
			
			tile.x = x;
			tile.y = y;
			tile.layerName = layerName;
			
			SetTile(tile);
			
#if UNITY_EDITOR
			//Debug.Log("Successfully added Tile from 2D ToolkitTileMap with Type: " + tile.GetType().ToString() + " on layer " + tile.layerName + " at " + tile.x + "/" + tile.y);
#endif
		}
		// Non Tk2D
		else
		{
			if(!IsIndexFree(tile.layerName, tile.x, tile.y)) return false;
			
			SetTile(tile);
			
#if UNITY_EDITOR
			//Debug.Log("Successfully added Tile with Type: " + tile.GetType().ToString() + " on layer " + tile.layerName + " at " + tile.x + "/" + tile.y);
#endif
		}
		
		return returnVar;
	}
	
	public static bool AddTile(BaseTileMapObject tile, int x, int y, string layerName)
	{
		if(!IsIndexFree(layerName, x, y)) return false;
		
		tile.x = x;
		tile.y = y;
		tile.layerName = layerName;
		
		SetTile(tile);
		
#if UNITY_EDITOR
		//Debug.Log("Successfully added Tile with Type: " + tile.GetType().ToString());
#endif
		
		return true;
	}
	
	public static bool RemoveTile(BaseTileMapObject tile)
	{
		if(IsIndexFree(tile.layerName, tile.x, tile.y)) return false;
		
		GetLayer(tile.layerName).Tiles[tile.x, tile.y] = null;
		
		_tilesList.Remove(tile);
		
		return true;
	}
	
	#endregion // Adding / Removing Methods
	
	#region Access Methods
	
	protected static TileMapLayer GetLayer(string layerName)
	{
		if(_layerList == null) throw new System.ArgumentNullException();
		
		TileMapLayer layer = null;
		_layerList.TryGetValue(layerName, out layer);
		
		return layer;
	}
	
	public static BaseTileMapObject GetTile(int layerDepth, int x, int y)
	{
		return GetTile(GetLayerNameFromLayerDepth(layerDepth), x, y);
	}
	
	public static BaseTileMapObject GetTile(string layerName, int x, int y)
	{
		x = GetCorrectedXIndex(x);
		y = GetCorrectedYIndex(y);
		
		BaseTileMapObject returnVar = null;
		
		var tempLayer = GetLayer(layerName);
		if(tempLayer != null)
		{
			returnVar = tempLayer.Tiles[x,y];
		}
		else
		{
			Debug.LogError("Null");
		}
		
		return returnVar;
	}
	
	public static List<BaseTileMapObject> GetTiles(int x, int y)
	{
		if(_layerList == null) throw new System.ArgumentNullException();
		
		List<BaseTileMapObject> tiles = new List<BaseTileMapObject>();
		
		foreach(var layer in _layerList.Values)
		{
			var tile = GetTile(layer.Name, x, y);
			if(tile != null)
			{
				tiles.Add(tile);
			}
		}
		
		return tiles;
	}
	
	public static Vector3 GetTilePosition(int x, int y)
	{
		var returnVar = Vector3.zero;
		var tiles = GetTiles(x, y);
		
		if(tiles.Count > 0)
		{
			returnVar = tiles[0].transform.position;
		}
		else
		{
			Debug.LogWarning("Found no Tile at this Index!");
		}
		
		return returnVar;
	}
	
	#endregion // Access Methods
	
	#region Manipulation Methods
	
	public static void SetTile(BaseTileMapObject tile)
	{
		var layer = GetLayer(tile.layerName);
		if(layer != null)
		{
			layer.Tiles[tile.x, tile.y] = tile;
		}
	}
	
	public static bool ChangeTileIndex(BaseTileMapObject tile, int newX, int newY)
	{
		if(_layerList == null) throw new System.ArgumentNullException();
		
		var returnVar = false;
		
		var layer = GetLayer(tile.layerName);
		if(layer != null && IsIndexFree(tile.layerName, newX, newY))
		{
			layer.Tiles[newX, newY] = tile;
			layer.Tiles[tile.x, tile.y] = null;
			tile.x = newX;
			tile.y = newY;
			returnVar = true;
		}
		else
		{
			Debug.LogError("Could not change Tile Index");
		}
		
		return returnVar;
	}
	/*
	public static bool ChangeTileIndexAndMoveGO(BaseTileMapObject tile, int newX, int newY)
	{
		var returnVar = ChangeTileIndex(tile, newX, newY);
		
		if(returnVar)
		{
			var targetPos = GetTilePosition(newX, newY);
			targetPos.z = tile.gameObject.transform.position.z;
			
			tile.gameObject.transform.position = targetPos;
		}
		
		return returnVar;
	}
	*/
	public static void ChangeTileLayer(BaseTileMapObject tile, string newLayerName)
	{
		if(tile.layerName != newLayerName && _layerList.Keys.Contains(newLayerName))
		{
#if UNITY_EDITOR
			Debug.Log("Changing Tile Layer of " + tile.name + " from " + tile.layerName + " to " + newLayerName);
#endif
			// layerName is valid and different
			tile.layerName = newLayerName;
			
			SetTile(tile);
		}
	}
	
	#endregion // Manipulation Methods
	
	#region Helper Methods
	
	public static bool IsIndexFree(string layerName, int x, int y)
	{
		var layer = GetLayer(layerName);
		if(layer != null)
		{
			if(layer.Tiles[x, y] == null)
			{
				return true;
			}
		}
		else
		{
			Debug.LogError("Null");
		}
		
		Debug.Log ("Layer " + layerName + " not free at " + x + "/" + y);
		
		return false;
	}
	
	public static float GetLayerDepthFromLayerName(string layerName)
	{
		var returnVar = 0f;
		
		if(_layerList == null) throw new System.ArgumentNullException();
		
		var layer = GetLayer(layerName);
		returnVar = layer.Depth;
		
		return returnVar;
	}
	
	public static string GetLayerNameFromLayerDepth(int depth)
	{
		var returnVar = "";
		
		if(_layerList == null) throw new System.ArgumentNullException();
		
		foreach(var layer in _layerList.Values)
		{
			if(layer.Depth == depth)
			{
				returnVar = layer.Name;
				break;
			}
		}
		
		return returnVar;
	}
	
	public static int GetCorrectedXIndex(int x)
	{
		var returnVar = x;
		
		if(returnVar < 0) returnVar = 0;
		if(returnVar >= Width) returnVar = Width - 1;
		
		return returnVar;
	}
	
	public static int GetCorrectedYIndex(int y)
	{
		var returnVar = y;
		
		if(returnVar < 0) returnVar = 0;
		if(returnVar >= Height) returnVar = Height - 1;
		
		return returnVar;
	}
	
	public static bool IsTileWalkable(BaseTileMapObject obj)
	{
		var returnVar = false;
		
		if(obj.GetType() == typeof(Tile) || obj.GetType().IsSubclassOf(typeof(Tile)))
		{
			var tile = (Tile)obj;
			
			returnVar = tile.IsWalkable;
		}
		
		return returnVar;
	}
	
	public static bool IsEveryTileWalkable(List<BaseTileMapObject> objList)
	{
		var returnVar = true;
		
		foreach (var obj in objList)
		{
			if(!IsTileWalkable(obj))
			{
				returnVar = false;
				break;
			}
		}
		
		return returnVar;
	}
	
	public static bool IsTileEnterableFromDirection(BaseTileMapObject obj, Direction dir)
	{
		var returnVar = false;
		
		if(obj.GetType() == typeof(Tile) || obj.GetType().IsSubclassOf(typeof(Tile)))
		{
			var tile = (Tile)obj;
			
			returnVar = tile.CanBeEnteredFrom(dir);
		}
		
		return returnVar;
	}
	
	public static bool IsEveryTileEnterableFromDirection(List<BaseTileMapObject> objList, Direction dir)
	{
		var returnVar = true;
		
		foreach (var obj in objList)
		{
			if(!IsTileEnterableFromDirection(obj, dir))
			{
				returnVar = false;
				break;
			}
		}
		
		return returnVar;
	}
	
	public static bool IsOnTileMap(int x, int y)
	{
		var returnVar = true;
		
		if(	(x < 0 || x > Width - 1) ||
			(y < 0 || y > Height - 1))
		{
			returnVar = false;
		}
		
		return returnVar;
	}
	
	#endregion // Helper Methods
}