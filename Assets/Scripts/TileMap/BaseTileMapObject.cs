using UnityEngine;


public class TileMovement
{
	public int FromX;
	public int FromY;
	public int ToX;
	public int ToY;
	
	
	public TileMovement(int fromX, int fromY, int toX, int toY)
	{
		FromX = fromX;
		FromY = fromY;
		ToX = toX;
		ToY = toY;
	}
	
	public bool TryGetDirection(out Direction dir)
	{
		dir = Direction.Left;
		
		// Has the Move a Direction?
		if(FromX - 1 == ToX) 
		{
			dir = Direction.Left;
			return true;
		}
		if(FromX + 1 == ToX) 
		{
			dir = Direction.Right;
			return true;
		}
		if(FromY - 1 == ToY) 
		{
			dir = Direction.Down;
			return true;
		}
		if(FromY + 1 == ToY) 
		{
			dir = Direction.Up;
			return true;
		}
		
		return false;
	}
}


public abstract class BaseTileMapObject : MonoBehaviour 
{
	[HideInInspector]
	public int x;
	[HideInInspector]
	public int y;
	[HideInInspector]
	public string layerName;
	
	
	void Awake () 
	{
		x = -1;
		y = -1;

		//TileMap.AddTile(this, out x, out y, out layerName);
		TileMap.RegisterTile(this);
		
		TileMap.TileMapInitializationCompleteEvent += OnTileMapInitializationComplete;
	}
	
	protected virtual void OnTileMapInitializationComplete()
	{
		#if UNITY_EDITOR
		Debug.Log(GetInfoString());
		#endif
	}
	
	public string GetInfoString()
	{
		return "I'm a: " + this.GetType().ToString() + " at x: " + x + " y: " + y + " Layer: " + layerName;
	}
}
