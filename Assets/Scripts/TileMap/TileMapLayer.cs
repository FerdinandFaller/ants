using UnityEngine;
using System.Collections;


public class TileMapLayer 
{
	public BaseTileMapObject[,] Tiles;
	public int Depth;
	public string Name;
	
	
	public TileMapLayer(string name, int width, int height, int depth)
	{
		Tiles = new BaseTileMapObject[width, height];
		Name = name;
		Depth = depth;
	}
}
