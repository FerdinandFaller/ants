using UnityEngine;
using System.Collections.Generic;


public class CommandManager : MonoBehaviour 
{
	private static List<ICommand> _commandsList;
	private static List<ICommand> _gatheredForMacroList;
	
	private static bool _isGatheringForMacro = false;
	
	
	void Awake()
	{
		_commandsList = new List<ICommand>();
		_gatheredForMacroList = new List<ICommand>();
	}
	
	void Start()
	{
		InputManager.InputEvent += OnInput;
	}
	
	
	private void OnInput(InputType inputType)
	{
		StartGatheringForMacro();
	}
	
	public static bool AddAndExecuteCommand(ICommand command)
	{
		var returnVar = false;
		
		if(!_commandsList.Contains(command))
		{
			_commandsList.Add(command);
			if(_isGatheringForMacro) GatherCommand(command);
			
			if(!command.Execute())
			{
				_commandsList.Remove(command);
				if(_isGatheringForMacro) _gatheredForMacroList.Remove(command);
				
				return false;
			}
			
			returnVar = true;
		}
		
		return returnVar;
	}
	
	public static bool UndoLastOfType(CommandType type)
	{
		if(_isGatheringForMacro) StopGatheringForMacro();
		
		var list = _commandsList;
		
		for(var i = list.Count - 1; i >= 0; i--)
		{
			if(list[i].CType == type && list[i].Undo())
			{
				list.RemoveAt(i);
				return true;
			}
		}
		
		return false;
	}
	
	public static bool UndoLast()
	{
		if(_commandsList.Count < 1) return false;
		if(_isGatheringForMacro) StopGatheringForMacro();
		
		var returnVar = _commandsList[_commandsList.Count-1].Undo();
		_commandsList.RemoveAt(_commandsList.Count-1);
		
		return returnVar;
	}
	
	public static void StartGatheringForMacro()
	{
		if(_isGatheringForMacro)
		{
			StopGatheringForMacro();
		}
		
		_isGatheringForMacro = true;
		
		Debug.Log ("Start Gathering");
	}
	
	public static void StopGatheringForMacro()
	{
		if(!_isGatheringForMacro || _gatheredForMacroList.Count == 0) return;
		
		_isGatheringForMacro = false;
		
		// Remove from CommandsList
		var index = _commandsList.IndexOf(_gatheredForMacroList[0]);
		_commandsList.RemoveRange(index, _gatheredForMacroList.Count);
	
		// Fill MacroCommand
		MacroCommand macro = new MacroCommand(_gatheredForMacroList);
		
		// Insert the Macro
		_commandsList.Insert(index, macro);
		
		Debug.Log ("Macro contains " + _gatheredForMacroList.Count + " commands");
		
		_gatheredForMacroList.Clear();
		
		Debug.Log ("Stop Gathering");
	}
	
	private static void GatherCommand(ICommand command)
	{
		if(command.CType == CommandType.CameraMovement) return;
		
		_gatheredForMacroList.Add(command);
	}
}
