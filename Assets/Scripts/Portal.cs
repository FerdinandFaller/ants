using UnityEngine;


public abstract class Portal : Tile 
{
	[HideInInspector]
	public Portal TargetPortal;
	
	public int Id;
	
	
	public override void Init ()
	{
		IsWalkable = true;
		CanBeEnteredFromLeft = true;
		CanBeEnteredFromRight = true;
		CanBeEnteredFromUp = true;
		CanBeEnteredFromDown = true;
		
		
		// Try to set the TargetPortal automatically
		AutoLinkPortals();
	}
	
	protected virtual void AutoLinkPortals()
	{
		if(TargetPortal == null)
		{
			var portals = GameObject.FindObjectsOfType(typeof(Portal)) as Portal[];
			
			foreach(var portal in portals)
			{
				// Itse me...
				if(portal == this || portal.GetType() != this.GetType()) continue;
				
				if(portal.Id == Id && portal.TargetPortal == null)
				{
					TargetPortal = portal;
					portal.TargetPortal = this;
					
					break;
				}
			}
		}
	}
}