using UnityEngine;


public class Ground : Tile
{
	public override void Init()
	{
		IsWalkable = true;
		CanBeEnteredFromLeft = true;
		CanBeEnteredFromRight = true;
		CanBeEnteredFromUp = true;
		CanBeEnteredFromDown = true;
	}
}
