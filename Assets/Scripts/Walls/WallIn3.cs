using UnityEngine;


[ExecuteInEditMode]
public class WallIn3 : TileIn3
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		CanBeEnteredFromDown = false;
		
		base.Init ();
	}
}
