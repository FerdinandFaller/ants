using UnityEngine;


[ExecuteInEditMode]
public class WallIn1 : TileIn1
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		CanBeEnteredFromDown = false;
		
		base.Init ();
	}
}
