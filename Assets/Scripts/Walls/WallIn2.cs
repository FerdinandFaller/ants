using UnityEngine;


[ExecuteInEditMode]
public class WallIn2 : TileIn2
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		CanBeEnteredFromDown = false;
		
		BaseSpriteId = "WallIn2";
		
		base.Init ();
	}
}
