using UnityEngine;


public class WallSingle : Tile
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		CanBeEnteredFromDown = false;
	}
}
