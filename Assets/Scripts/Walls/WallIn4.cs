using UnityEngine;


public class WallIn4 : TileIn4
{
	public override void Init()
	{
		IsWalkable = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		CanBeEnteredFromDown = false;
	}
}
