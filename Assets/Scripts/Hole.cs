using UnityEngine;
using System.Collections;


/// <summary>
/// The used GFX without rotation has to come in from Left.
/// </summary>
[ExecuteInEditMode]
public class Hole : Portal 
{
	public bool WasUsed;
	public Direction UsedDirection;
	
	private tk2dSprite _sprite;
	
	
	public override void Init ()
	{
		base.Init ();
		
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		
		if(WasUsed)
		{
			SetUsedState(UsedDirection);
		}
	}
	
	public void SetUsedState(Direction dir)
	{
		WasUsed = true;
		UsedDirection = dir;
		IsWalkable = false;
		CanBeEnteredFromDown = false;
		CanBeEnteredFromLeft = false;
		CanBeEnteredFromRight = false;
		CanBeEnteredFromUp = false;
		
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		_sprite.spriteId = _sprite.GetSpriteIdByName("HoleUsed");
		
		UpdateGFX(dir);
	}
	
	public void SetUnUsedState()
	{
		WasUsed = false;
		IsWalkable = true;
		CanBeEnteredFromDown = true;
		CanBeEnteredFromLeft = true;
		CanBeEnteredFromRight = true;
		CanBeEnteredFromUp = true;
		
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		_sprite.spriteId = _sprite.GetSpriteIdByName("Hole");
	}
	
	public void UpdateGFX(Direction inDir)
	{
		var zAngle = 0;
		
		switch(inDir)
		{
			case Direction.Left:
			{
				zAngle = 0;
			}break;
			
			case Direction.Right:
			{
				zAngle = 180;
			}break;
			
			case Direction.Up:
			{
				zAngle = -90;
			}break;
			
			case Direction.Down:
			{
				zAngle = 90;
			}break;
		}
		
		transform.Rotate(-transform.rotation.eulerAngles);
		transform.rotation = Quaternion.Euler(0, 0, zAngle);
	}
	
#if UNITY_EDITOR
	void Update()
	{
		if(WasUsed && _sprite.spriteId != _sprite.GetSpriteIdByName("HoleUsed"))
		{
			SetUsedState(UsedDirection);
		}
	}
#endif
}
