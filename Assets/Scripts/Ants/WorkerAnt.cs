using UnityEngine;


public class WorkerAnt : Ant 
{
	public override void Init ()
	{
		IsWalkable = true;
		CanBeEnteredFromLeft = true;
		CanBeEnteredFromRight = true;
		CanBeEnteredFromUp = true;
		CanBeEnteredFromDown = true;
	}
	/*
	public override void BeEaten ()
	{
		if(TileMap.RemoveTile(this))
		{
#if UNITY_EDITOR
			Debug.Log(this.GetType().ToString() + " was eaten");
#endif
			// TODO: CHANGE HUNGER
			
			Destroy(gameObject);
		}
	}*/
}
