using UnityEngine;
using System.Collections.Generic;


public class BuilderAnt : Ant 
{
	public int MovesPerShield = 5;
	public Direction FirstPriority = Direction.Up;
	public Direction SecondPriority = Direction.Right;
	public Direction ThirdPriority = Direction.Down;
	public Direction FourthPriority = Direction.Left;
	
	public GameObject ShieldPrefab;
	
	private int _totalMovesCount;
	private bool _didBuildShieldThisMove;
	private TongueTip _tongueTip;
	
	
	#region Inherited Methods
	
	public override void Init ()
	{
		IsWalkable = true;
		CanBeEnteredFromLeft = true;
		CanBeEnteredFromRight = true;
		CanBeEnteredFromUp = true;
		CanBeEnteredFromDown = true;
		
		if(_tongueTip == null)
		{
			var tongueGO = GameObject.FindGameObjectWithTag("TongueTip");
			_tongueTip = tongueGO.GetComponent<TongueTip>();
			if(_tongueTip == null)
			{
				Debug.LogError("Could not find the TongueTip!");
			}
			else
			{
				_tongueTip.MoveEvent += OnTongueMovement;
				_tongueTip.MoveUndoneEvent += OnTongueMovementUndone;
			}
		}
	}
	
	protected override void OnTileMapInitializationComplete ()
	{
		base.OnTileMapInitializationComplete ();
		
		if(_tongueTip == null)
		{
			var tongueGO = GameObject.FindGameObjectWithTag("TongueTip");
			_tongueTip = tongueGO.GetComponent<TongueTip>();
			if(_tongueTip == null)
			{
				Debug.LogError("Could not find the TongueTip!");
			}
			else
			{
				_tongueTip.MoveEvent += OnTongueMovement;
				_tongueTip.MoveUndoneEvent += OnTongueMovementUndone;
			}
		}
	}
	
	protected void OnTongueMovement(TileMovement move, int moveCount)
	{
		_didBuildShieldThisMove = false;
		_totalMovesCount = moveCount;
	}
	
	protected void OnTongueMovementUndone(TileMovement move, int moveCount)
	{
		_didBuildShieldThisMove = true;
		
		_totalMovesCount = moveCount;
	}
	
	void OnDisable()
	{
		_tongueTip.MoveEvent -= OnTongueMovement;
		_tongueTip.MoveUndoneEvent -= OnTongueMovementUndone;
	}
	
	void OnEnable()
	{
		if(_tongueTip != null)
		{
			_tongueTip.MoveEvent += OnTongueMovement;
			_tongueTip.MoveUndoneEvent += OnTongueMovementUndone;
		}
	}
	
	#endregion // Inherited Methods
	
	
	void Update()
	{
		if(_totalMovesCount % MovesPerShield == 1 && !_didBuildShieldThisMove && _totalMovesCount > 1)
		{
			_didBuildShieldThisMove = true;
			
			List<Direction> neededDirections = new List<Direction>();
			
			Debug.Log("Should build new Shield on Tile: " + x + " / " + y);
			
			// TODO:
			// Build new Shield
			// Check if and where this Tile already has a non walkable neighboring Tile (The ant doesn't need to build a shield in that direction)
			// Left
			if(IsTileNextToUsWalkable(Direction.Left)) neededDirections.Add(Direction.Left);
			// Right
			if(IsTileNextToUsWalkable(Direction.Right)) neededDirections.Add(Direction.Right);
			// Up
			if(IsTileNextToUsWalkable(Direction.Up)) neededDirections.Add(Direction.Up);
			// Down
			if(IsTileNextToUsWalkable(Direction.Down)) neededDirections.Add(Direction.Down);
			
			Debug.Log("Needed Dirs: " + neededDirections.Count.ToString());
			
			// Check if and in which direction(s) the Tile the ant is on already has (a) shield(s). Is one of them in the list? remove it from the list
			var tiles = TileMap.GetTiles(x,y);
			List<Shield> shields = new List<Shield>();
			foreach(var tile in tiles)
			{
				if(tile.GetType() == typeof(Shield))
				{
					var shield = (Shield)tile;
					shields.Add(shield);
					if(shield.IsLeft && neededDirections.Contains(Direction.Left))
					{
						neededDirections.Remove(Direction.Left);
					}
					if(shield.IsRight && neededDirections.Contains(Direction.Right))
					{
						neededDirections.Remove(Direction.Right);
					}
					if(shield.IsUp && neededDirections.Contains(Direction.Up))
					{
						neededDirections.Remove(Direction.Up);
					}
					if(shield.IsDown && neededDirections.Contains(Direction.Down))
					{
						neededDirections.Remove(Direction.Down);
					}
				}
			}
			
			Debug.Log("Needed Dirs: " + neededDirections.Count.ToString());
			Debug.Log("shields.Count: " + shields.Count.ToString());
			
			// Do we have at least one shield on this Tile?
			Shield usedShield = null;
			if(shields.Count > 0)
			{
				// Yes, use the first one
				usedShield = shields[0];
			}
			else
			{
				// No, create one
				if(CommandManager.AddAndExecuteCommand(new BuildShieldCommand(x, y, ShieldPrefab)))
				{
					// And get it
					foreach(var tile in TileMap.GetTiles(x,y))
					{
						if(tile.GetType() == typeof(Shield) || tile.GetType().IsSubclassOf(typeof(Shield)))
						{
							usedShield = (Shield)tile;
						}
					}
				}
			}
			
			// All remaining Directions in the List need a Shield. Builders build Clockwise.
			if(neededDirections.Contains(FirstPriority))
			{
				CommandManager.AddAndExecuteCommand(new EnableShieldDirectionCommand(usedShield, FirstPriority));
			}
			else if(neededDirections.Contains(SecondPriority))
			{
				CommandManager.AddAndExecuteCommand(new EnableShieldDirectionCommand(usedShield, SecondPriority));
			}
			else if(neededDirections.Contains(ThirdPriority))
			{
				CommandManager.AddAndExecuteCommand(new EnableShieldDirectionCommand(usedShield, ThirdPriority));
			}
			else if(neededDirections.Contains(FourthPriority))
			{
				CommandManager.AddAndExecuteCommand(new EnableShieldDirectionCommand(usedShield, FourthPriority));
			}
		}
	}
	
	private bool IsTileNextToUsWalkable(Direction dir)
	{
		var returnVar = false;
		int index;
		
		switch(dir)
		{
			case Direction.Left:
			{
				index = TileMap.GetCorrectedXIndex(x - 1);
				if(index == x - 1)
				{
					if(TileMap.IsEveryTileWalkable(TileMap.GetTiles(index, y)))
					{
						returnVar = true;
					}
				}
			}break;
			
			case Direction.Right:
			{
				index = TileMap.GetCorrectedXIndex(x + 1);
				if(index == x + 1)
				{
					if(TileMap.IsEveryTileWalkable(TileMap.GetTiles(index, y)))
					{
						returnVar = true;
					}
				}
			}break;
			
			case Direction.Up:
			{
				index = TileMap.GetCorrectedXIndex(y + 1);
				if(index == y + 1)
				{
					if(TileMap.IsEveryTileWalkable(TileMap.GetTiles(x, index)))
					{
						returnVar = true;
					}
				}
			}break;
			
			case Direction.Down:
			{
				index = TileMap.GetCorrectedXIndex(y - 1);
				if(index == y - 1)
				{
					if(TileMap.IsEveryTileWalkable(TileMap.GetTiles(x, index)))
					{
						returnVar = true;
					}
				}
			}break;
			
			default:
			{
				
			}break;
		}
		
		return returnVar;
	}
}
