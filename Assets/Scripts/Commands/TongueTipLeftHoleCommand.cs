using UnityEngine;


public class TongueTipLeftHoleCommand : Command 
{
	private TongueTip _tongueTip;
	private Hole _hole;
	
	
	public TongueTipLeftHoleCommand(TongueTip tongueTip, Hole hole, CommandType type = CommandType.TileMovement) : base(type)
	{
		_tongueTip = tongueTip;
		_hole = hole;
	}
	
	public override bool Execute()
	{	
		_hole.SetUsedState(_tongueTip.LookDirection);
		_tongueTip.ChangeAppearanceToNormal();
		_tongueTip.IsInHole = false;
		
		return true;
	}
	
	public override bool Undo()
	{
		_hole.SetUnUsedState();
		_tongueTip.ChangeAppearenceToLookOutOfHole();
		_tongueTip.IsInHole = true;
		
		Debug.Log("Undo TipLeftHole");
		
		return true;
	}
}
