using UnityEngine;


public class TongueTipMoveLeftCommand : Command 
{
	private TongueTipMoveCommand _tongueMoveCommand;
	
	
	public TongueTipMoveLeftCommand(TongueTip tongueTip, CommandType type = CommandType.TileMovement) : base(type)
	{
		int targetX = tongueTip.x;
		int targetY = tongueTip.y;
		
		targetX = tongueTip.x - 1;
		
		var move = new TileMovement(tongueTip.x, tongueTip.y, targetX, targetY);
		
		if(CType != CommandType.TileMovement)
		{
			_tongueMoveCommand = new TongueTipMoveCommand(tongueTip, move, type);
		}
		else
		{
			_tongueMoveCommand = new TongueTipMoveCommand(tongueTip, move);
		}
	}
	
	public override bool Execute()
	{
		return _tongueMoveCommand.Execute();
	}
	
	public override bool Undo()
	{
		return _tongueMoveCommand.Undo();
	}
}
