using UnityEngine;


public class DisableShieldDirectionCommand : Command 
{
	private Shield _shield;
	private Direction _dir;
	
	
	public DisableShieldDirectionCommand(Shield shield, Direction dir, CommandType type = CommandType.TileManipulation) : base(type)
	{
		_shield = shield;
		_dir = dir;
	}
	
	public override bool Execute()
	{
		if(_shield != null && _shield.IsDirectionEnabled(_dir))
		{
			_shield.SetDirection(_dir, false);
			return true;
		}
		
		return false;
	}
	
	public override bool Undo()
	{
		if(_shield != null)
		{
			_shield.SetDirection(_dir, true);
			return true;
		}
		
		return false;
	}
}
