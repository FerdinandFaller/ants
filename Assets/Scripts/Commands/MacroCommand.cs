using UnityEngine;
using System.Collections.Generic;


public class MacroCommand : Command 
{
	private List<ICommand> _commands;
	
	
	public MacroCommand(IEnumerable<ICommand> commands, CommandType type = CommandType.Macro) : base(type)
	{
		_commands = new List<ICommand>();
		_commands.AddRange(commands);
	}
	
	public override bool Execute()
	{
		if(_commands == null) return false;
		
		var returnVar = true;
		foreach(var command in _commands)
		{
			if(!command.Execute())
			{
				returnVar = false;
			}
		}
		
		return returnVar;
	}
	
	public override bool Undo()
	{
		if(_commands == null) return false;
		
		var temp = _commands;
		temp.Reverse();
		
		var returnVar = true;
		foreach(var command in temp)
		{
			if(!command.Undo())
			{
				returnVar = false;
			}
		}
		
		return returnVar;
	}
}
