using UnityEngine;


public class EatAntCommand : Command 
{
	private Ant _ant;
	
	
	public EatAntCommand(Ant ant, CommandType type = CommandType.TileRemoval) : base(type)
	{
		_ant = ant;
	}
	
	public override bool Execute()
	{
		if(_ant != null)
		{
			if(TileMap.RemoveTile(_ant))
			{
				_ant.gameObject.active = false;
				// TODO: Hunger!
				return true;
			}
		}
		
		return false;
	}
	
	public override bool Undo()
	{
		if(TileMap.AddTile(_ant))
		{
			_ant.gameObject.active = true;
			// TODO: Hunger!
			return true;
		}
		
		return false;
	}
}
