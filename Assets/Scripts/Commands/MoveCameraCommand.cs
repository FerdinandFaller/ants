using UnityEngine;


public class MoveCameraCommand : Command 
{
	private Vector3 _from, _to;
	private float _time;
	private iTween.EaseType _easeType;
	private CameraController _cameraController;
	
	
	public MoveCameraCommand(CameraController cameraController, float time, iTween.EaseType easeType, Vector3 fromPos, Vector3 toPos, CommandType type = CommandType.CameraMovement) : base(type)
	{
		_cameraController = cameraController;
		_from = fromPos;
		_to = toPos;
		_easeType = easeType;
		_time = time;
	}
	
	public override bool Execute()
	{
		_cameraController.MoveCameraTo(_to, _time, _easeType);
		return true;
	}
	
	public override bool Undo()
	{
		_cameraController.MoveCameraTo(_from, _time, _easeType);
		return true;
	}
}
