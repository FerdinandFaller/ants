using UnityEngine;


public class AddTongueBaseToTileMapCommand : Command 
{
	private GameObject _tongueBasePrefab;
	private int _x, _y;
	private Direction _in, _out;
	private static GameObject _parentGO;
	
	
	public AddTongueBaseToTileMapCommand(GameObject tongueBasePrefab, int x, int y, Direction inDir, Direction outDir, CommandType type = CommandType.TileAdding) : base(type)
	{
		_tongueBasePrefab = tongueBasePrefab;
		_x = x;
		_y = y;
		_in = inDir;
		_out = outDir;
		
		if(_parentGO == null)
		{
			_parentGO = GameObject.Find("TongueBaseGOs");
			
			if(_parentGO == null)
			{
				_parentGO = new GameObject("TongueBaseGOs");
			}
		}
	}
	
	public override bool Execute()
	{
		if(_in == _out) return false;
		
		var worldPos = TileMap.GetTilePosition(_x, _y);
		worldPos.z = TileMap.GetLayerDepthFromLayerName(TileMap.Instance.TongueBaseLayerName);
		GameObject tBaseGO = (GameObject)GameObject.Instantiate(_tongueBasePrefab, worldPos, Quaternion.identity);
		tBaseGO.transform.parent = _parentGO.transform;
		
		if(tBaseGO != null)
		{
			var tBase = tBaseGO.GetComponent<TongueBase>();
			tBase.x = _x;
			tBase.y = _y;
			tBase.layerName = TileMap.Instance.TongueBaseLayerName;
			tBase.In1 = _in;
			tBase.In2 = _out;
			tBase.Init();
			
			// Add the Base to the TileMap
			if(!TileMap.AddTile(tBase))
			{
				Debug.LogWarning("Could not add Tile " + tBase.name + " to the TileMap on layer " + tBase.layerName + " at " + tBase.x + "/" + tBase.y );
				GameObject.Destroy(tBaseGO);
			}
			else
			{
				return true;
			}
		}
		
		return false;
	}
	
	public override bool Undo()
	{
		var returnVar = false;
		
		var tile = TileMap.GetTile(TileMap.Instance.TongueBaseLayerName, _x, _y);
		var tileGO = tile.gameObject;
		
		if(tile != null && tileGO != null && TileMap.RemoveTile(tile))
		{
			GameObject.Destroy(tileGO);
			returnVar = true;
		}
		
		return returnVar;
	}
}
