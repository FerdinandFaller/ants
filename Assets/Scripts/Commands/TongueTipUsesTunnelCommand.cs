using UnityEngine;


public class TongueTipUsesTunnelCommand : Command 
{
	private TongueTip _tongueTip;
	private Tunnel _tunnel;
	
	
	public TongueTipUsesTunnelCommand(TongueTip tongueTip, Tunnel tunnel, CommandType type = CommandType.TileMovement) : base(type)
	{
		_tongueTip = tongueTip;
		_tunnel = tunnel;
	}
	
	public override bool Execute()
	{
		var returnVar = false;
		
		returnVar = TileMap.ChangeTileIndex(_tongueTip, _tunnel.TargetPortal.x, _tunnel.TargetPortal.y);
		
		if(returnVar)
		{
			var targetPos = TileMap.GetTilePosition(_tunnel.TargetPortal.x, _tunnel.TargetPortal.y);
			targetPos.z = _tongueTip.gameObject.transform.position.z;
			_tongueTip.gameObject.transform.position = targetPos;
			
			Direction in1, in2;
			in1 = _tongueTip.LookDirection;
			in2 = GlobalEnums.GetOppositeOfDirection(_tongueTip.LookDirection);
			
			CommandManager.AddAndExecuteCommand(new AddTongueBaseToTileMapCommand(_tongueTip.TongueBasePrefab, _tunnel.x, _tunnel.y, in1, in2));
		}
		
		return returnVar;
	}
	
	public override bool Undo()
	{
		var returnVar = false;
		
		if(TileMap.ChangeTileIndex(_tongueTip, _tunnel.x, _tunnel.y))
		{
			var targetPos = TileMap.GetTilePosition(_tunnel.x, _tunnel.y);
			targetPos.z = _tongueTip.gameObject.transform.position.z;
			_tongueTip.gameObject.transform.position = targetPos;
			
			returnVar = true;
		}
		
		return returnVar;
	}
}
