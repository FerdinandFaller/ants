
public class NullCommand : Command
{
	public NullCommand() : base(CommandType.Null)
	{
		
	}
	
	public override bool Execute()
	{
		return true;
	}
	
	public override bool Undo()
	{
		return true;
	}
}
