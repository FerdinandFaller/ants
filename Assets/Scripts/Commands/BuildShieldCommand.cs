using UnityEngine;
using System.Collections.Generic;


public class BuildShieldCommand : Command 
{
	private int _x, _y;
	private GameObject _shieldPrefab;
	private Shield _shield;
	private static GameObject _parentGO;
	
	
	public BuildShieldCommand(int x, int y, GameObject shieldPrefab, CommandType type = CommandType.TileManipulation) : base(type)
	{
		_shieldPrefab = shieldPrefab;
		_x = x;
		_y = y;
		
		if(_parentGO == null)
		{
			_parentGO = GameObject.Find("ShieldGOs");
			
			if(_parentGO == null)
			{
				_parentGO = new GameObject("ShieldGOs");
			}
		}
	}
	
	public override bool Execute()
	{
		var pos = TileMap.GetTilePosition(_x, _y);
		pos.z = TileMap.GetLayerDepthFromLayerName(TileMap.Instance.ObstaclesLayerName);
		GameObject go = (GameObject)GameObject.Instantiate(_shieldPrefab, pos, Quaternion.identity);
		go.transform.parent = _parentGO.transform;
		var shield = go.GetComponent<Shield>();
		shield.IsLeft = false;
		shield.IsRight = false;
		shield.IsUp = false;
		shield.IsDown = false;
		
		if(!TileMap.AddTile(shield, _x, _y, TileMap.Instance.ObstaclesLayerName))
		{
			Debug.LogWarning("I really wanted to build a shield, but the TileMap won't let me");
			GameObject.Destroy(go);
		}
		else
		{
			_shield = shield;
			return true;
		}
		
		return false;
	}
	
	public override bool Undo()
	{
		var returnVar = false;
		
		if(_shield != null)
		{
			var shieldGO = _shield.gameObject;
			
			if(shieldGO != null && TileMap.RemoveTile(_shield))
			{
				GameObject.Destroy(shieldGO);
				returnVar = true;
			}
		}
		
		return returnVar;
	}
}
