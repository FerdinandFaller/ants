using UnityEngine;


public class TongueTipEnteredHoleCommand : Command 
{
	private TongueTip _tongueTip;
	private Hole _hole;
	
	
	public TongueTipEnteredHoleCommand(TongueTip tongueTip, Hole hole, CommandType type = CommandType.TileMovement) : base(type)
	{
		_tongueTip = tongueTip;
		_hole = hole;
	}
	
	public override bool Execute()
	{
		var returnVar = false;
		
		returnVar = TileMap.ChangeTileIndex(_tongueTip, _hole.TargetPortal.x, _hole.TargetPortal.y);
		
		if (returnVar)
		{
			var targetPos = TileMap.GetTilePosition(_hole.TargetPortal.x, _hole.TargetPortal.y);
			targetPos.z = _tongueTip.gameObject.transform.position.z;
			_tongueTip.gameObject.transform.position = targetPos;
			
			_hole.SetUsedState(GlobalEnums.GetOppositeOfDirection(_tongueTip.LookDirection));
			_tongueTip.IsInHole = true;
			_tongueTip.ChangeAppearenceToLookOutOfHole();
		}
		
		return returnVar;
	}
	
	public override bool Undo()
	{
		var returnVar = false;
		
		if(TileMap.ChangeTileIndex(_tongueTip, _hole.x, _hole.y))
		{
			var targetPos = TileMap.GetTilePosition(_hole.x, _hole.y);
			targetPos.z = _tongueTip.gameObject.transform.position.z;
			_tongueTip.gameObject.transform.position = targetPos;
			
			_hole.SetUnUsedState();
			_tongueTip.IsInHole = false;
			_tongueTip.ChangeAppearanceToNormal();
			returnVar = true;
			
			Debug.Log("Undo TipEnteredHole");
		}
		
		return returnVar;
	}
}
