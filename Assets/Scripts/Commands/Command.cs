
public abstract class Command : ICommand
{
	public CommandType CType {get; private set;}
	
	
	public Command(CommandType type)
	{
		CType = type;
	}
	
	public abstract bool Execute();
	public abstract bool Undo();
}
