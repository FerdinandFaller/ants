using UnityEngine;


public class TongueTipMoveCommand : Command 
{
	protected TongueTip TongueTip{get; private set;}
	protected int TargetX {get; private set;}
	protected int TargetY {get; private set;}
	private TileMovement _move;
	private TileMovement _prevMove;
	private Direction _prevLookDir;
	
	
	public TongueTipMoveCommand(TongueTip tongueTip, TileMovement move, CommandType type = CommandType.TileMovement) : base(type)
	{
		TongueTip = tongueTip;
		TargetX = move.ToX;
		TargetY = move.ToY;
		_move = move;
		_prevMove = TongueTip.LastMove;
		_prevLookDir = TongueTip.LookDirection;
	}
	
	public override bool Execute()
	{
		// If the TargetTile is valid, get it from the TileMap
		if(!TileMap.IsOnTileMap(TargetX, TargetY)) return false;
		
		var targetTiles = TileMap.GetTiles(TargetX, TargetY);
		
		if(!TileMap.IsEveryTileWalkable(targetTiles)) return false;
		
		// Has the Move a Direction?
		System.Nullable<Direction> dir = null;
		if(TargetX == TongueTip.x - 1) dir = Direction.Left;
		if(TargetX == TongueTip.x + 1) dir = Direction.Right;
		if(TargetY == TongueTip.y - 1) dir = Direction.Down;
		if(TargetY == TongueTip.y + 1) dir = Direction.Up;
		if(dir != null)
		{
			if(!TileMap.IsEveryTileEnterableFromDirection(targetTiles, GlobalEnums.GetOppositeOfDirection(dir.Value))) return false;
			
			TongueTip.LookDirection = dir.Value;
		}
		
		// Keep the old index a little
		var oldX = TongueTip.x;
		var oldY = TongueTip.y;
		var oldTiles = TileMap.GetTiles(oldX, oldY);
		
		// Move the TongueTip to the TargetTile
		if(TileMap.ChangeTileIndex(TongueTip, TargetX, TargetY))
		{
			var targetPos = TileMap.GetTilePosition(TargetX, TargetY);
			targetPos.z = TongueTip.gameObject.transform.position.z;
			TongueTip.gameObject.transform.position = targetPos;
		}
		else
		{
			return false;
		}
		
		// Eventually leave a TongueBase behind, we need the old index now
		if(!TongueTip.IsInHole)
		{
			Direction in1, in2;
			_move.TryGetDirection(out in1);
			
			if(TongueTip.LastMove == null || !TongueTip.LastMove.TryGetDirection(out in2))
			{
				in2 = TongueTip.LookDirection;
			}
			
			in2 = GlobalEnums.GetOppositeOfDirection(in2);
			
			CommandManager.AddAndExecuteCommand(new AddTongueBaseToTileMapCommand(TongueTip.TongueBasePrefab, oldX, oldY, in1, in2));
		}
		else
		{
			// We just left a Hole
			foreach(var tile in oldTiles)
			{
				if(tile.GetType() == typeof(Hole) || tile.GetType().IsSubclassOf(typeof(Hole)))
				{
					var holeTile = (Hole)tile;
					CommandManager.AddAndExecuteCommand(new TongueTipLeftHoleCommand(TongueTip, holeTile));
				}
			}
		}
		
		// Check if the TargetTiles are Ants
		foreach(var tile in targetTiles)
		{
			if(tile.GetType() == typeof(Ant) || tile.GetType().IsSubclassOf(typeof(Ant)))
			{
				var antTile = (Ant)tile;
				CommandManager.AddAndExecuteCommand(new EatAntCommand(antTile));
			}
		}
		
		// Check if the TargetTiles are Portals
		foreach(var tile in targetTiles)
		{
			ICommand command = new NullCommand();
				
			// Holes
			if(tile.GetType() == typeof(Hole) || tile.GetType().IsSubclassOf(typeof(Hole)))
			{
				var holeTile = (Hole)tile;
				command = new TongueTipEnteredHoleCommand(TongueTip, holeTile);
			}
			// Tunnels
			else if(tile.GetType() == typeof(Tunnel) || tile.GetType().IsSubclassOf(typeof(Tunnel)))
			{
				var tunnelTile = (Tunnel)tile;
				command = new TongueTipUsesTunnelCommand(TongueTip, tunnelTile);
			}
			
			CommandManager.AddAndExecuteCommand(command);
		}
		
		// Check if we ran over any Shields and disable them
		foreach(var tile in oldTiles)
		{
			if(tile.GetType() == typeof(Shield) || tile.GetType().IsSubclassOf(typeof(Shield)))
			{
				var shield = (Shield)tile;
				CommandManager.AddAndExecuteCommand(new DisableShieldDirectionCommand(shield, TongueTip.LookDirection));
			}
		}
		
		TongueTip.MovementsCount++;
		TongueTip.FireMoveEvent(_move);
		TongueTip.LastMove = _move;
		
		return true;
	}
	
	public override bool Undo()
	{
		var returnVar = false;
		
		if(TileMap.ChangeTileIndex(TongueTip, _move.FromX, _move.FromY))
		{
			var targetPos = TileMap.GetTilePosition(_move.FromX, _move.FromY);
			targetPos.z = TongueTip.gameObject.transform.position.z;
			TongueTip.gameObject.transform.position = targetPos;
			
			returnVar = true;
			TongueTip.MovementsCount--;
			TongueTip.LastMove = _prevMove;
			TongueTip.LookDirection = _prevLookDir;
			TongueTip.FireMoveUndoneEvent(_move);
			
			Debug.Log("Undo TipMoved");
		}
		
		return returnVar;
	}
}
