public interface ICommand 
{
	CommandType CType {get;}
	
	bool Execute();
	bool Undo();
}
