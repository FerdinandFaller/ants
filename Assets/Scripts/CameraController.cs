using UnityEngine;


public class CameraController : MonoBehaviour 
{
	private TongueTip _tongueTip;
	[HideInInspector]
	public Vector3 TargetPos;
	
	
	void Awake()
	{
		if(_tongueTip == null) _tongueTip = FindTongueTip();
	}
	
	void Start()
	{
		PositionAtTileMapCenter();
		
		MoveCameraTo(_tongueTip.gameObject.transform.position, 5f, iTween.EaseType.easeInOutSine);
	}
	
	void OnDisable()
	{
		if(_tongueTip == null) return;
		
		_tongueTip.MoveEvent -= OnTongueMovement;
		_tongueTip.MoveUndoneEvent -= OnTongueMovementUndone;
	}
	
	void OnEnable()
	{
		if(_tongueTip == null) _tongueTip = FindTongueTip();
		
		if(_tongueTip != null)
		{
			_tongueTip.MoveEvent += OnTongueMovement;
			_tongueTip.MoveUndoneEvent += OnTongueMovementUndone;
		}
	}
	
	protected void OnTongueMovement(TileMovement move, int moveCount)
	{
		MoveCameraTo(_tongueTip.gameObject.transform.position, 1f, iTween.EaseType.easeOutSine);
	}
	
	protected void OnTongueMovementUndone(TileMovement move, int moveCount)
	{
		MoveCameraTo(_tongueTip.gameObject.transform.position, 1f, iTween.EaseType.easeOutSine);
	}
	
	public void MoveCameraTo(Vector3 pos, float time, iTween.EaseType easeType)
	{
		TargetPos = pos;
		iTween.MoveTo(	gameObject, 
						iTween.Hash("position", pos, 
									"time", time,
									"easetype", easeType));
	}
	
	public void MoveCameraTo(Vector3 pos)
	{
		TargetPos = pos;
		gameObject.transform.position = TargetPos;
	}
	
	public void MoveCameraToTargetPos(float time, iTween.EaseType easeType)
	{
		MoveCameraTo(TargetPos, time, easeType);
	}
	
	private TongueTip FindTongueTip()
	{
		var tongueTipGO = GameObject.Find("TongueTipLeft 1");
		if(tongueTipGO == null) tongueTipGO = GameObject.Find("TongueTipRight 1");
		if(tongueTipGO == null) tongueTipGO = GameObject.Find("TongueTipUp 1");
		if(tongueTipGO == null) tongueTipGO = GameObject.Find("TongueTipDown 1");
		
		if(tongueTipGO == null) throw new System.NullReferenceException();
		
		return tongueTipGO.GetComponent<TongueTip>();
	}
	
	private void PositionAtTongueTip()
	{
		var pos = _tongueTip.gameObject.renderer.bounds.center;
		pos.z = Camera.main.transform.position.z;
		Camera.main.transform.position = pos;
		
		Debug.Log(pos.ToString());
	}
	
	private void PositionAtTileMapCenter()
	{
		var groundGO = GameObject.Find("Ground 1");
		if(groundGO == null) throw new System.NullReferenceException();
		var pos = new Vector3(	groundGO.renderer.bounds.extents.x * TileMap.Width, 
								groundGO.renderer.bounds.extents.y * TileMap.Height, 0);
		pos.z = Camera.main.transform.position.z;
		Camera.main.transform.position = pos;
		
		Debug.Log(pos.ToString());
	}
}
