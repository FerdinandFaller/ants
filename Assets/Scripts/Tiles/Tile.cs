using UnityEngine;

[ExecuteInEditMode]
public abstract class Tile : BaseTileMapObject
{
	public bool IsWalkable = true;
	
	public bool CanBeEnteredFromLeft = true;
	public bool CanBeEnteredFromRight = true;
	public bool CanBeEnteredFromUp = true;
	public bool CanBeEnteredFromDown = true;
	
	
	void Start()
	{
		Init ();
	}
	
	public abstract void Init();
	
	public bool CanBeEnteredFrom(Direction direction)
	{
		switch(direction)
		{
			case Direction.Left:
			{
				return CanBeEnteredFromLeft;
			}break;
			
			case Direction.Right:
			{
				return CanBeEnteredFromRight;
			}break;
			
			case Direction.Up:
			{
				return CanBeEnteredFromUp;
			}break;
			
			case Direction.Down:
			{
				return CanBeEnteredFromDown;
			}break;
			
			default:
			{
				return false;
			}break;
		}
	}
}
