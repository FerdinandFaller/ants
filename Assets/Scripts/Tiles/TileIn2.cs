using UnityEngine;

/// <summary>
/// TileIn2 represents Tiles where the GFX seems to come in from two sides. 
/// E.G. a middle part of a wall. It is not possible to set In1 and In2 to the same Direction.
/// The used Curve GFX without rotation has to go from Left to Down.
/// The used Straight GFX without rotation has to be horizontal.
/// </summary>

[ExecuteInEditMode]
public abstract class TileIn2 : Tile
{
	public Direction In1 = Direction.Left;
	public Direction In2 = Direction.Right;
	
	/// <summary>
	/// The BaseSpriteId will be combined with either "Straight" or "Curve" to choose the according Sprite 
	/// in the SpriteCollection. (Make sure there are Sprites labeled that way in the used Collection).
	/// </summary>
	protected string BaseSpriteId;
	
	protected tk2dSprite _sprite;
	
	
	public override void Init()
	{
		_sprite = gameObject.GetComponent<tk2dSprite>();
		
		UpdateGFX(In1, In2);
	}
	
	public void UpdateGFX(Direction inDir, Direction outDir)
	{
		if(inDir == outDir) return;
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		
		transform.Rotate(-transform.rotation.eulerAngles);
		
		switch(In1)
		{
			case Direction.Left:
			{
				switch(In2)
				{
					case Direction.Right:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Straight");
					}break;
				
					case Direction.Up:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, -90);
					}break;
				
					case Direction.Down:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
					}break;
					
					default:
					{
						UpdateGFX(inDir, GlobalEnums.GetOppositeOfDirection(inDir));
					}break;
				}
			}break;
			
			case Direction.Right:
			{
				switch(In2)
				{
					case Direction.Left:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Straight");
					}break;
				
					case Direction.Up:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, -180);
					}break;
				
					case Direction.Down:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, 90);
					}break;
					
					default:
					{
						UpdateGFX(inDir, GlobalEnums.GetOppositeOfDirection(inDir));
					}break;
				}
			}break;
			
			case Direction.Up:
			{
				switch(In2)
				{
					case Direction.Left:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, -90);
					}break;
				
					case Direction.Right:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, -180);
					}break;
				
					case Direction.Down:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Straight");
						transform.rotation = Quaternion.Euler(0, 0, -90);
					}break;
					
					default:
					{
						UpdateGFX(inDir, GlobalEnums.GetOppositeOfDirection(inDir));
					}break;
				}
			}break;
			
			case Direction.Down:
			{
				switch(In2)
				{
					case Direction.Left:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
					}break;
				
					case Direction.Right:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Curve");
						transform.rotation = Quaternion.Euler(0, 0, 90);
					}break;
				
					case Direction.Up:
					{
						_sprite.spriteId = _sprite.GetSpriteIdByName(BaseSpriteId + "Straight");
						transform.rotation = Quaternion.Euler(0, 0, -90);
					}break;
					
					default:
					{
						UpdateGFX(inDir, GlobalEnums.GetOppositeOfDirection(inDir));
					}break;
				}
			}break;
			
			default:
			{
				return;
			}break;
		}
	}
}
