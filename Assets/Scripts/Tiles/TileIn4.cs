using UnityEngine;

/// <summary>
/// TileIn4 represents Tiles where the GFX seems to come in from four sides.
/// </summary>
[ExecuteInEditMode]
public abstract class TileIn4 : Tile 
{
	// This is the same as a normal Tile and exists only for convinience reasons.
}
