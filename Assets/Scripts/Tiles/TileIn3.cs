using UnityEngine;

/// <summary>
/// TileIn3 represents Tiles where the GFX seems to come in from three sides. 
/// 
/// The middle In of the used GFX without rotation has to come in from Left.
/// </summary>
[ExecuteInEditMode]
public abstract class TileIn3 : TileIn1
{
	// This is the same as TileIn1 and exists only for convinience reasons.
}
