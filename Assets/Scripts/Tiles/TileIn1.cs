using UnityEngine;

/// <summary>
/// TileIn1 represents Tiles where the GFX seems to come in from one side. 
/// E.G. an end part of a wall.
/// The used GFX without rotation has to come in from Left.
/// </summary>
[ExecuteInEditMode]
public class TileIn1 : Tile 
{
	public Direction In = Direction.Left;
	
	protected tk2dSprite _sprite;
	
	
	public override void Init()
	{
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		
		UpdateGFX(In);
	}
	
	public void UpdateGFX(Direction inDir)
	{
		if(_sprite == null) _sprite = gameObject.GetComponent<tk2dSprite>();
		
		var zAngle = 0;
		
		switch(inDir)
		{
			case Direction.Left:
			{
				zAngle = 0;
			}break;
			
			case Direction.Right:
			{
				zAngle = 180;
			}break;
			
			case Direction.Up:
			{
				zAngle = -90;
			}break;
			
			case Direction.Down:
			{
				zAngle = 90;
			}break;
		}
		
		transform.Rotate(-transform.rotation.eulerAngles);
		transform.rotation = Quaternion.Euler(0, 0, zAngle);
	}
}
