using UnityEngine;
using System.Collections.Generic;


public class InputManager : MonoBehaviour 
{
	public GameState CurrentGameState{get; private set;} // TODO: This should be somewhere else
	
	public static event System.Action<InputType> InputEvent;
	
	private List<Vector3> _inputPosList;
	private TongueTip _tongueTip;
	private CameraController _cameraController;
	private bool _isDragging;
	private Vector3 _scrollTargetPos;
	
	
	void Awake()
	{
		_inputPosList = new List<Vector3>();
		
		_tongueTip = GameObject.FindGameObjectWithTag("TongueTip").GetComponent<TongueTip>();
		
		_cameraController = Camera.main.gameObject.GetComponent<CameraController>();
	}
	
	void Update()
	{
		switch(CurrentGameState)
		{
			case GameState.Ingame:
			{
				Ingame();
			}break;
			
			default:
			{
			}break;
		}
	}
	
	private void Ingame()
	{
		_inputPosList = GenerateInputPosList() as List<Vector3>;
		
		//TODO: Camerascrolling doesn't work correctly
		
		/*
		if(!_isDragging)
		{
			if(!TongueTipMovement())
			{
				CameraScrolling();
			}
		}
		else
		{
			CameraScrolling();
		}
		*/
		
		TongueTipMovement();
	}
	
	private bool CameraScrolling()
	{
		var returnVar = false;
		
		if(_isDragging)
		{
			// No Input, stop dragging
			if(_inputPosList.Count <= 0)
			{
				_isDragging = false;
			}
			else
			{
				// Keep on dragging!
				_cameraController.MoveCameraTo(_scrollTargetPos);
				
				returnVar = true;
			}
		}
		else
		{
			if(_inputPosList.Count > 0)
			{
				_isDragging = true;
				
				_scrollTargetPos = Camera.main.ScreenToWorldPoint(_inputPosList[0]);
				
				// Start dragging!
				_cameraController.MoveCameraTo(_scrollTargetPos);
				
				returnVar = true;
			}
		}
		
		return returnVar;
	}
	
	private bool TongueTipMovement()
	{
		_inputPosList.Clear();
		
		// Mouse
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
		{
			_inputPosList.Add(Input.mousePosition);
			Debug.Log (Input.mousePosition.ToString());
			if(InputEvent != null)
			{
				InputEvent(InputType.Mouse);
			}
		}
		// Touch
		else if (Input.touchCount > 0)
		{
			// Get all touches
			foreach(var touch in Input.touches)
			{
				_inputPosList.Add(touch.position);
			}
			
			if(InputEvent != null)
			{
				InputEvent(InputType.Touch);
			}
		}
		
		System.Nullable<Direction> dir = null;
		foreach(var inputPos in _inputPosList)
		{
			// TongueTip Direction
			Ray ray = Camera.main.ScreenPointToRay(inputPos);
			RaycastHit hitInfo;
	        if (Physics.Raycast(ray, out hitInfo) && hitInfo.transform.tag == "Ground")
			{
				var ground = hitInfo.transform.gameObject.GetComponent<Ground>();
				if(ground.x == _tongueTip.x - 1 && ground.y == _tongueTip.y) dir = Direction.Left;
				else if(ground.x == _tongueTip.x + 1 && ground.y == _tongueTip.y) dir = Direction.Right;
				else if(ground.y == _tongueTip.y + 1 && ground.x == _tongueTip.x) dir = Direction.Up;
				else if(ground.y == _tongueTip.y - 1 && ground.x == _tongueTip.x) dir = Direction.Down;
			}
			break;
		}
		
		// Keyboard
		if(	Input.GetKeyDown(KeyCode.LeftArrow) ||
			Input.GetKeyDown(KeyCode.RightArrow) ||
			Input.GetKeyDown(KeyCode.UpArrow) ||
			Input.GetKeyDown(KeyCode.DownArrow))
		{
			// TongueTip Direction
			if(Input.GetKeyDown(KeyCode.LeftArrow)) dir = Direction.Left;
			if(Input.GetKeyDown(KeyCode.RightArrow)) dir = Direction.Right;
			if(Input.GetKeyDown(KeyCode.UpArrow)) dir = Direction.Up;
			if(Input.GetKeyDown(KeyCode.DownArrow)) dir = Direction.Down;
			
			if(InputEvent != null)
			{
				InputEvent(InputType.Keyboard);
			}
		}
		
		if(dir != null)
		{
			switch (dir)
			{
				case Direction.Left:
				{
					CommandManager.AddAndExecuteCommand(new TongueTipMoveLeftCommand(_tongueTip));
				}break;
				
				case Direction.Right:
				{
					CommandManager.AddAndExecuteCommand(new TongueTipMoveRightCommand(_tongueTip));
				}break;
				
				case Direction.Up:
				{
					CommandManager.AddAndExecuteCommand(new TongueTipMoveUpCommand(_tongueTip));
				}break;
				
				case Direction.Down:
				{
					CommandManager.AddAndExecuteCommand(new TongueTipMoveDownCommand(_tongueTip));
				}break;
			}
			
			return true;
		}
		
		return false;
	}
	
	private IEnumerable<Vector3> GenerateInputPosList()
	{
		var inputPosList = new List<Vector3>();
		
		// Mouse
		if(Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
		{
			inputPosList.Add(Input.mousePosition);
		}
		// Touch
		else if (Input.touchCount > 0)
		{
			// Get all touches
			foreach(var touch in Input.touches)
			{
				inputPosList.Add(touch.position);
			}
		}
		
		return inputPosList;
	}
}
