using UnityEngine;
using System.Collections;
using iGUI;

public class iGUICode_TestGUI : MonoBehaviour
{
	[HideInInspector]
	public iGUIButton button2;
	[HideInInspector]
	public iGUIButton button1;
	[HideInInspector]
	public iGUIContainer container1;
	[HideInInspector]
	public iGUIRoot root1;

	static iGUICode_TestGUI instance;
	
	
	void Awake()
	{
		instance=this;
	}
	public static iGUICode_TestGUI getInstance()
	{
		return instance;
	}

	public void button1_ClickUp(iGUIButton caller)
	{
		Application.LoadLevel("Test");
	}

	public void button2_ClickUp(iGUIButton caller)
	{
		CommandManager.UndoLast();
	}

}
