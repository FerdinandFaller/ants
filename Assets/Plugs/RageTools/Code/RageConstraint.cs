using UnityEngine;

/// <summary> Constrains (follows) a certain object transform position, rotation and scale, selectively </summary>
[AddComponentMenu("RageTools/Rage Constraint")]
[ExecuteInEditMode]
public class RageConstraint : MonoBehaviour {

	[SerializeField]private GameObject _follower;
	public GameObject Follower {
		get { return _follower; }
		set {
			if (_follower == value) return;
			_follower = value;
			FollowerGroup = _follower.GetComponent<RageGroup>();
		}
	}
	public bool GroupVisible {
		get {
			if (FollowerGroup == null) return false;
			return FollowerGroup.Visible;
		}
		set {
			if (FollowerGroup == null) return;
			FollowerGroup.Visible = value;
		}
	}
	public bool FollowPosition;
	public bool FollowRotation;
	public bool FollowScale;
	public bool Local;
	public RageGroup FollowerGroup;
	private RageEdgetune _edgetune;
	public bool Live;

	public void Update() {
		UpdateFollowerTransform();
		if (!Live || Follower == null) return;
		if (_edgetune == null)
			_edgetune = Follower.GetComponent<RageEdgetune>();
		if (_edgetune == null) return;
		_edgetune.Group.QueueRefresh();
	}

	private void UpdateFollowerTransform( ) {
		if (!Live) return;
		if (!Follower) return;

		if (FollowPosition)
			CopyPosition();

		if (FollowRotation) {
			CopyRotation();
		}

		if (FollowScale) {
			if (!Follower.transform.localScale.Equals (transform.localScale))
				Follower.transform.localScale = transform.localScale;
			//TODO: .lossyScale
		}
	}

	private void CopyPosition( ) {
		if (Local) {
			if (!Follower.transform.localPosition.Equals(transform.localPosition))
				Follower.transform.localPosition = transform.localPosition;
			return;
		}
		if (!Follower.transform.position.Equals (transform.position))
			Follower.transform.position = transform.position;
	}

	private void CopyRotation( ) {
		if (Local) {
			if (!Follower.transform.localRotation.Equals(transform.localRotation))
				Follower.transform.localRotation = transform.localRotation;
			return;
		}
		if (!Follower.transform.rotation.Equals (transform.rotation))
			Follower.transform.rotation = transform.rotation;
	}

}
