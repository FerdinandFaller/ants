﻿using UnityEngine;

public class RageSvgObject : ScriptableObject {

	public GameObject Object;
	public ISpline Spline;
	public Transform Parent;
	public int PointIdx;
	public Vector3 CursorPos;

	private RageSvgObject() {
		Object = null;
		Spline = null;
		Parent = null;
		PointIdx = 0;
		CursorPos = Vector3.zero;
	}

	public static RageSvgObject NewInstance() {
		return (RageSvgObject)CreateInstance(typeof(RageSvgObject));
	}
}
